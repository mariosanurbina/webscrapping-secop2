# -*- encoding: utf-8 -*-
import csv
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from bs4 import BeautifulSoup
import time
import mysql.connector
from mysql.connector import errorcode
import mysql
from datetime import datetime
from datetime import timedelta
import logging

def funcionImpresionLogs(mensaje):
    logger1 =logging.getLogger()
    logger1.warning(mensaje)

def arreglarcampomonto(monto):
    LargoMonto = len(monto)
    QuitarCOP = monto[0:LargoMonto-4]
    QuitarComas = QuitarCOP.split(",")
    MontoArreglado = ''.join(QuitarComas)
    return MontoArreglado

def FiltrarFilasconFechasViejas(fila):
    TresMeses = date.today()-timedelta(days=90)
    if fila[5] > TresMeses:
        return fila
    else:
        pass

def ArreglarDiayMesEnFecha(fecha):
    if len(fecha) == 2:
        NewFecha = fecha
        return NewFecha
    else:
        NewFecha = "0"+fecha
        return NewFecha


def arreglarcampofecha(fecha):
    if len(fecha) > 1:
        QuitarAdicionales = fecha.split(" ")
        FechaMod = QuitarAdicionales[0].split("/")
        FechaNuevoFormato = FechaMod[2]+"-"+ArreglarDiayMesEnFecha(FechaMod[0])+"-"+ArreglarDiayMesEnFecha(FechaMod[1])
        return FechaNuevoFormato
    else:
        return NULL

def quitarNone(s):
    if s is None:
        return ''
    else:
        return s

def ModificarCampoLinkyOtros(array):
    NuevoBloque =[]
    for fila in array:
        NuevaFila =[]
        for elemento in fila[0:5]:
            NuevaFila.append(elemento)
        for elemento in fila[5:6]:
            NuevaFila.append(arreglarcampofecha(elemento))
        for elemento in fila[6:7]:
            NuevaFila.append(elemento)
        for elemento in fila[7:8]:
            NuevaFila.append(arreglarcampomonto(elemento))
        for elemento in fila[8:9]:
            NuevaFila.append(elemento)
        for elemento in fila[10:11]:
            linkMod = elemento.split("+")
            NuevaFila.append("https://community.secop.gov.co/Public/Tendering/OpportunityDetail/Index?noticeUID="+ linkMod[3][2:len(linkMod[3])-2] +"&isFromPublicArea=True&isModal=true&asPopupView=true")
            NuevaFila.append(linkMod[3][2:len(linkMod[3])-2])
        for elemento in fila[11:16]:
            NuevaFila.append(elemento)
        NuevoBloque.append(NuevaFila)

    return NuevoBloque

def obtenertabladeregistrosporcodigo(html,code):
    bs = BeautifulSoup(html, 'html.parser')
    table = bs.find(id="tblMainTable_trRowMiddle_tdCell1_tblForm_trGridRow_tdCell1_grdResultList_tbl")
    #bodys = table.findAll('tbody')
    rows= table.findAll('tr')
    c=len(rows)-2
    Bloque =[]
    for row in rows[1:c]:
        fila = []
        fila.append(code)


        for cell in row.findAll('td')[1:len( row.findAll('td'))]:
            fila.append(quitarNone(cell.get_text()))
        fila.append(row.find('a').attrs['onclick'])
        fila.append("Colombia")
        fila.append("Fecha de publicación")
        fila.append("Modalidad")
        fila.append("SECOP-DOS" )
        fila.append(time.strftime("%Y-%m-%d") )
        Bloque.append(fila)
    return ModificarCampoLinkyOtros(Bloque)


def navergarEnPaginaSeleniumPrimerasAcciones(browser):
    BusquedaAvanzada = browser.find_element_by_id("lnkAdvancedSearch")
    BusquedaAvanzada.click()
    time.sleep(5)
    estatus = Select(browser.find_element_by_id("selRequestStatus"))
    estatus.select_by_visible_text("Published")
    time.sleep(5)




def NavegarEnPaginaConSeleniumCiclos(TipoNegocio,codGeneral,browser):

    categoria = browser.find_element_by_id("btnMainCategoryButton")
    categoria.click()
    time.sleep(3)
    browser.switch_to_frame(0)
    cajatexto=browser.find_element_by_id("txtQuickSearchText_CategoryTreeView")
    cajatexto.send_keys(TipoNegocio)
    go=browser.find_element_by_id("btnQuickSearch_CategoryTreeView")
    go.click()
    time.sleep(6)
    casilla = browser.find_element_by_partial_link_text(TipoNegocio)
    casilla.click()
    time.sleep(5)
    botonOK = browser.find_element_by_name("btnOK")
    botonOK.click()
    time.sleep(5)
    BotonBuscar = browser.find_element_by_id("btnSearchButton")
    BotonBuscar.click()
    time.sleep(10)
    try:
        MoreItems = browser.find_element_by_partial_link_text("More Items").is_displayed()
        if MoreItems == True:
            while MoreItems == True:
                #MoreItems = browser.find_element_by_partial_link_text("More Items").is_displayed()
                MoreItems1 = browser.find_element_by_partial_link_text("More Items")
                MoreItems1.click()
                time.sleep(25)
            return (obtenertabladeregistrosporcodigo(browser.page_source, codGeneral))
    except:
        tabla = browser.find_element_by_id("tblMainTable_trRowMiddle_tdCell1_tblForm_trGridRow")
        if len(tabla.text) >15:
            return (obtenertabladeregistrosporcodigo(browser.page_source, codGeneral))
        else:
            pass

def cargar_datos_mysql(array,cnx1):
      navegar = cnx1.cursor()
      navegar.execute("INSERT IGNORE INTO secop2 (id,link,sector,secop,identificador,modalidad,estado,entidad,descripcion,region,monto,tipofecha,fecha,fechareplicas,fechaingreso) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" ,(array[10], array[9], array[0],array[14], array[2], array[13], array[4],array[1], array[3], array[11], array[7], array[12],array[5],array[6],array[15] ))
      cnx1.commit()



#configuracion de logs
logging.basicConfig(filename ='logssecop2.log',level= logging.WARNING, format ='%(asctime)s:%(levelname)s:%(message)s')
logger =logging.getLogger()


#ejecución del programa


with open ('valoresdos222.csv','r') as valoresdos:
    codigo = csv.DictReader(valoresdos)

    try:
      cnx = mysql.connector.connect(user='root', password='',database='bitnami_wordpress')

    except mysql.connector.Error as err:
      if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        funcionImpresionLogs("Algo esta mal con el name or password")
      elif err.errno == errorcode.ER_BAD_DB_ERROR:
        funcionImpresionLogs("La base de datos no existe")
      else:
        funcionImpresionLogs(err)

    else:

        #navegación incial

        browser1 = webdriver.Chrome( executable_path ='/usr/bin/chromedriver')
        url = 'https://community.secop.gov.co/Public/Tendering/ContractNoticeManagement/Index?currentLanguage=en&Page=login&Country=CO&SkinName=CCE'
        try:
            browser1.get(url)
        except:
            funcionImpresionLogs("Hay un problema con la conexión al SECOP2")
        else:
            navergarEnPaginaSeleniumPrimerasAcciones(browser1)
            for negocio in codigo:
                Acumulado= NavegarEnPaginaConSeleniumCiclos(negocio['cod_detallado'],negocio['cod_general'],browser1)
                LargoCadena = len(Acumulado)
                if LargoCadena >0:
                    #cargar_datos_mysql(Acumulado)
                    for linea in Acumulado:
                        TresMeses = datetime.now()-timedelta(days=90)   #TresMeses = datetime.date.today()-timedelta(days=90)
                        establecerFecha = datetime.strptime(linea[5],'%Y-%m-%d')
                        if establecerFecha > TresMeses:
                            cargar_datos_mysql(linea,cnx)
                        else:
                            pass
                else:
                    continue
        browser1.close()
        cnx.close()
